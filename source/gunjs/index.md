---
layout: page
title: gunjs
date: 2018-06-05 12:56:32
tags:
---

# Gun.js


## Setup:
 There are three setup or more depend on format on javascripts. Note that nodejs and javascript browser client as some differents. Nodejs can save files and client browser can only save data on the browser local storage database.

## Basic
 Standard setup on how server and client works just gun.js.

### Nodejs server:
 ```javascript
    var Gun = require('gun');

 ```
### Browser client:
 ```javascript
    var gun = GUN();
 ```

## Advance
 This required few more setup to simple file from low to high layer scripting. To able to compile files to high to low script languages. It depend on webpack, babel, typescript, coffee, gulp or grunt build scripts. This section is base on babel javascript.

### Nodejs Babel server:
 ```javascript
    import gun from 'gun'; //for server nodejs format

 ```

### Nodejs Babel browser client:
 ```javascript
    import gun from 'gun/gun';// for client javacript format

 ```