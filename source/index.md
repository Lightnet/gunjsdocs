title: hexo-theme-doc-seed
---

# Gunjs Docs

This project is documentation for gun.js.

## Getting Started
 Gun is graph database, peer to peer and small javascript file. It can run online and offline browser / client and server side of javascript. It can be stand alone on browser to local storage data.


## More Informations

For more informations please visit the [user documentation website](https://gun.eco/docs/Introduction).
