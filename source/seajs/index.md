---
layout: page
title: seajs
date: 2018-06-05 12:57:13
tags:
---

# SEA API
 Sea API is authority and encrypt as well an utilities helper. To develop graph database check for user write access auth to prevent imposter overriding data. By using Gun.js over lays sea.js auth add ons.

 Notes:
  * This API will change since it being worked on to improve auth.

## SEA User:
```html
<script src="gun/gun.js"></script>
<script src="gun/sea.js"></script>
```

```javascript
//localStorage.clear(); //clear database for gun
var gun =  Gun();
var user = gun.user();
//check if user exist
if(user.is){
    console.log('user!');
}
//create user
user.create("test","test",function(ack){
	console.log("created!", ack.pub);
});
//login check
user.auth("test", "test",function(ack){
    if(ack.err){
        console.log("fail!");
    }else{
        console.log("Authorized!");
    }
});
//logout
user.leave();
//change password
user.auth("test", "test",function(ack){//current password
    if(ack.err){
        console.log("fail!");
    }else{
        console.log("Password Change!");
    }
},{change:"newtest"});//new password chanage
```

```javascript
    var gun =  Gun();
    var user = gun.user('alias public key'); //get user data
```

## SEA Utilities functions:
```javascript
var SEA = Gun.SEA;
SEA.pair(); //random generate keys
SEA.encrypt(data, pair);
SEA.sign(data, pair);
SEA.verify(data, pair);
SEA.decrypt(data, pair);
SEA.work(data, pair);
```